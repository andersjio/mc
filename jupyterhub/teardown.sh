kubectl delete service -n juphub --all
kubectl delete pod -n juphub --all
kubectl delete deployment.apps/juphub -n juphub
kubectl delete rolebinding juphub-view  --namespace=juphub
kubectl delete rolebinding juphub-edit  --namespace=juphub