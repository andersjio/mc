kubectl create rolebinding juphub-view   --clusterrole=view   --serviceaccount=juphub:default   --namespace=juphub
kubectl create rolebinding juphub-edit   --clusterrole=edit   --serviceaccount=juphub:default   --namespace=juphub
kubectl apply -f jupyter-hub.yaml
kubectl apply -f jupyter-service.yaml